<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\MiningController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Laravel\Sanctum\Sanctum;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', [MiningController::class, 'getApplicants'])->middleware('auth:sanctum');


Route::post('/logout', [LoginController::class, 'logout']);


Route::resource('minings', MiningController::class);

Route::get('/applicants', [MiningController::class, 'getApplicants']);

Route::get('/renew-lisence/{id}', [MiningController::class, 'getApplicants']);

Route::get('/weekly-lisence', [MiningController::class, 'getWeeklyLisence']);

Route::get('/weekly-applicant', [MiningController::class, 'getWeeklyApplicant']);

Route::get('/weekly-lisence', [MiningController::class, 'getWeeklyRenewed']);

Route::get('/dashboard', [MiningController::class, 'dashboard']);


Route::get('/total-applicants', [MiningController::class, 'getTotalApplicants']);

Route::get('/total-lisences', [MiningController::class, 'getTotalLisences']);

Route::get('/total-renewed', [MiningController::class, 'getTotalRenewed']);

Route::get('/total-expired', [MiningController::class, 'getTotalExpired']);

Route::get('/active-lisence', [MiningController::class, 'getActiveLisence']);


Route::get('/renew-lisence', [MiningController::class, 'getRenewedLisence']);

Route::get('/lisence/{id}', [MiningController::class, 'showLisence']);

Route::get('/renew-lisencelist/{id}', [MiningController::class, 'getRenewedLisenceList']);

Route::get('/edit-renewal/{id}', [MiningController::class, 'updateRenewal']);

Route::get('/renew-lisence/{id}', [MiningController::class, 'renewLisence']);

Route::get('/expired-lisence', [MiningController::class, 'getExpiredLisence']);

Route::post('/applicant/create', [MiningController::class, 'create']);

Route::post('/generate-lisence', [MiningController::class, 'generateLisence']);

Route::post('/renewlisence/{id}', [MiningController::class, 'updatedRenewal']);

Route::post('/storerenewal/{id}', [MiningController::class, 'storeRenewal']);

Route::get('/count', [MiningController::class, 'getCount']);

Route::get('/users', [MiningController::class, 'getUser']);

Route::post('/user/store', [MiningController::class, 'storeUser']);

Route::post('/user/update', [MiningController::class, 'updateUser']);

Route::post('/delete-user/{id}',[MiningController::class, 'deleteUser']);





Route::get('/diffdate', [MiningController::class, 'diffDate']);


Route::get('/search-active/{any?}', [MiningController::class, 'searcActive']);

Route::get('/search-applicants/{any?}', [MiningController::class, 'searchApplicants']);

Route::get('/search-expired/{any?}', [MiningController::class, 'searchExpired']);

Route::get('/search-renewed/{any?}', [MiningController::class, 'searchRenewed']);

Route::get('/pdf',[MiningController::class, 'getPdf']);


Route::post('/update-lisence/{id}',[MiningController::class, 'updateLisence']);

Route::post('/delete-lisence/{id}',[MiningController::class, 'deleteLisence']);

Route::post('/delete-renewal/{id}',[MiningController::class, 'deleteRenewal']);

Route::get('/monthly',[MiningController::class, 'monthly']);
