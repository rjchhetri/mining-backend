<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('sd');
            $table->string('village');
            $table->string('houseno');
            $table->string('telephone');
            $table->string('contact');
            $table->string('status');
            $table->boolean('lisence_renewed');
            $table->string('sector_type');
            $table->string('business_profession');
            $table->string('mining_documents');
            $table->string('minerals_intended');
            $table->string('mining_period');
            $table->string('area_extent');
            $table->string('area_details');
            $table->boolean('under_forest')->default(false);
            $table->string('forest_division')->nullable();
            $table->string('legal_status')->nullable();
            $table->boolean('sanctuary')->nullable();
            $table->string('type_extent')->nullable();
            $table->string('sketch_plan')->nullable();
            $table->string('already_hold_already');
            $table->string('already_applied_not_granted');
            $table->string('applied_simultaneously');
            $table->string('captive_use');
            $table->string('if_sale');
            $table->string('financial_resource');
            $table->string('other_particulars')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants');
    }
}
