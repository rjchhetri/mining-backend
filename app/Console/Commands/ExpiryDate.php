<?php

namespace App\Console\Commands;

use App\Models\Applicant;
use App\Models\Lisence;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ExpiryDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expiry:date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function sendSMS($contact) {
        $basic  = new \Vonage\Client\Credentials\Basic("6a081506", "Zt5XCbSiCccFiwsI");
        $client = new \Vonage\Client($basic);

        $response = $client->sms()->send(
            new \Vonage\SMS\Message\SMS('91'.$contact, 'BRAND_NAME', 'Your licence is expiring soon. Please renew now !')
        );
        
        $message = $response->current();
        
        if ($message->getStatus() == 0) {
            echo "The message was sent successfully to: ".$contact;
        } else {
            echo "The message failed with status: " . $message->getStatus() . "\n";
        }
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        $expiry =  Lisence::where('expiry_date',Carbon::now()->addDays(3)->toDateString())->first();
        // code to send sms
        if($expiry) {
              // code to send sms
              $applicant = Applicant::where('id',$expiry->applicant_id)->first();

              $this->sendSMS($applicant->contact);
        }

        Lisence::where('expiry_date',Carbon::now()->toDateString())
                ->update([
                    'status' => 'expired'
                ]);
    }
}
