<?php

namespace App\Console\Commands;

use App\Models\Applicant;
use App\Models\Lisence;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateName extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;

        // $applicant =  Applicant::find(1);

        // $applicant->name = 'worked';

        // $applicant->save();

        Lisence::where('status', 'active')
            ->update([
                'status' => 'test'
            ]);

        return;
            
        


        $expiry =  Lisence::where('expiry_date',Carbon::now()->subDays(3)->toDateString())->first();

            if($expiry) {
                Lisence::where('id',$expiry->id)
                        ->update(['status' => 'expired']);
            }
    }
}
