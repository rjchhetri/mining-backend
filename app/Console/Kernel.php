<?php

namespace App\Console;

use App\Models\Applicant;
use App\Models\Lisence;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\ExpiryDate',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $dt = Carbon::now();


        // $schedule->call(function () {
        //     $applicant =  Applicant::find(1);

        //     $applicant->name = 'worked';

        //     $applicant->save();

           
        // })->everyMinute();
        
        // $schedule->call(function () {
        //     $expiry =  Lisence::where('expiry_date',Carbon::now()->subDays(3)->toDateString())->first();

        //     if($expiry) {
        //         Lisence::where('id',$expiry->id)
        //                 ->update(['expiry_date' => Carbon::now()]);
        //     }
        // })->everyMinute();

        $schedule->command('update:name')->everyMinute();

        // $schedule->call(new DeleteRecentUsers)->daily();


        // $dt = Carbon::now();

    
        //check if the current subscription is expired
       
            
        //     Subscription::where('expires', '<=', $dt->toDateString())
        //                     ->update(['status'=> 'expired']);

        //     Transaction::where('expires', '<=', $dt->toDateString())
        //                     ->update(['status'=> 'expired']);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
