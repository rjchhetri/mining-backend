<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Applicant;
use App\Models\CooperativeSector;
use App\Models\FirmAssociationSector;
use App\Models\IndividualSector;
use App\Models\Lisence;
use App\Models\PrivateSector;
use App\Models\PublicSector;
use App\Models\Renewal;
use App\Models\Test;
use App\Models\User;
// use Barryvdh\DomPDF\PDF;
// use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Dotenv\Validator;
use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\In;

class MiningController extends Controller
{
    private $id, $hello;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {

    //     $dt = Carbon::now();

    //     Lisence::where('expiry_date', '<=', $dt->toDateString())
    //         ->update(['status' => 'expired']);
    // }

    public function updateLisence(Request $request, $id)
    {
        // 'email' => 'required|unique:users,email,'.$user->id,
        $lisence = Lisence::where('applicant_id', $id)->first();

        $request->validate([
            'lisence_id' => 'required|unique:lisences,lisence_id,' . $lisence->id,
        ]);

        // return back()->withErrors([
        //     'email' => 'The provided credentials do not match our records.',
        // ]);

        $lisence = Lisence::where('applicant_id', $id)->first();

        $file = $this->generatePdf($request->name, $request->contact, Carbon::parse($lisence->expiry_date), $request->lisence_id);


        // return $file;

        Lisence::where('applicant_id', $id)
            ->update([
                'lisence_id' => $request->lisence_id,
                'file' => $file
            ]);


        Applicant::where('id', $id)
            ->update([
                'name' => $request->name,
                'village' => $request->village,
                'district' => $request->district,
                'contact' => $request->contact,
            ]);
    }

    public function deleteUser($id)
    {
        User::where('id', $id)->delete();
    }

    public function deleteLisence($id)
    {
        // return $id;
        if ($id == 'all')
            Lisence::truncate();
        else
            Lisence::where('applicant_id', $id)->delete();
    }

    public function deleteRenewal($id)
    {
        $renewal = Renewal::find($id);

        $lisence = Lisence::where('applicant_id', $renewal->applicant_id)->first();

        $lisence->lisence_renewed = $lisence->lisence_renewed - 1;

        $lisence->save();

        $renewal->delete();
    }

    public function getUser()
    {
        return User::all();
    }

    public function storeUser(Request $request)
    {

        // return $request->email;
        $user =  new User();

        $user->name = $request->name;

        $user->email = $request->email;

        $user->password = Hash::make($request->password);

        $user->roles = $request->roles;

        $user->save();
    }

    public function updateUser(Request $request)
    {
        // return $request->all();
        // return $request->password ? $request->password : 'null' ; 
        $user = User::find($request->id);

        $user->name = $request->name;

        $user->email = $request->email;

        $user->roles = $request->roles;

        // $request->password = $request->password ? Hash::make($request->password) : $user->password; 

        if ($request->password)
            $user->password = Hash::make($request->password);

        $user->save();
    }

    public function getPdf()
    {
        // $filePath_mining_documents = $request->file('mining_documents')->store('images', ['disk' => 'public']);
        $lisence = "ABC" . date("Y") . Str::random(4) . rand(100, 200);

        return strtoupper($lisence);


        $lisence = "DGM"  . date("Y") . rand(100, 200);

        // return 'test';


        $current_date = Carbon::now()->addYear(1)->toDateString();

        $pdf = PDF::loadHTML("
                <style>
                .card {
                  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                  max-width: 300px;
                  margin: auto;
                  text-align: center;
                  font-family: arial;
                  background-color: rgb(248, 248, 248);
                }
                
                .title {
                  color: grey;
                  font-size: 18px;
                }
                
                button {
                  border: none;
                  outline: 0;
                  display: inline-block;
                  padding: 8px;
                  color: white;
                  background-color: #000;
                  text-align: center;
                  cursor: pointer;
                  width: 100%;
                  font-size: 18px;
                }
                
                a {
                  text-decoration: none;
                  font-size: 22px;
                  color: black;
                }
                
                button:hover, a:hover {
                  opacity: 0.7;
                }
                </style>
                </head>
                <body>
                
                
                
                <div class='card'>
                  <img src='https://www.ritiriwaz.com/wp-content/uploads/2017/01/Indian-Emblem.jpg' alt='John' style='width:50%'>
                  <h4>Name: Hello </h4>
                  <h4>Contact: 8888 </h4>
                  <h4>Lisence no: $lisence </h4>
                  <h4>Valid till: $current_date </h4>
                  <p><button>DGM Mizoram</button></p>
                </div>
                
                ");


        // $filePath_mining_documents = $pdf->store('images', ['disk' => 'public']);




        $content = $pdf->download('blabla.pdf')->getOriginalContent();

        // $filePath_mining_documents = $request->file('sketch_plan')->store('images', ['disk' => 'public']);

        // return Storage::put('/images/'+$request->contact+'.pdf',$content);

        $random = "8888" . Str::random(10);

        return Storage::put("/pdf/$random.pdf", $content);




        $data = [

            'title' => 'Welcome to HDTuto.com',

            'date' => date('m/d/Y')

        ];


        $name = 'Rj Chhetri';

        $contact = "68668";

        $current_date = Carbon::now()->addYear(1)->toDateString();

        $pdf = PDF::loadHTML("
        <style>
        .card {
          box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
          max-width: 300px;
          margin: auto;
          text-align: center;
          font-family: arial;
          background-color: rgb(248, 248, 248);
        }
        
        .title {
          color: grey;
          font-size: 18px;
        }
        
        button {
          border: none;
          outline: 0;
          display: inline-block;
          padding: 8px;
          color: white;
          background-color: #000;
          text-align: center;
          cursor: pointer;
          width: 100%;
          font-size: 18px;
        }
        
        a {
          text-decoration: none;
          font-size: 22px;
          color: black;
        }
        
        button:hover, a:hover {
          opacity: 0.7;
        }
        </style>
        </head>
        <body>
        
        
        
        <div class='card'>
          <img src='https://www.ritiriwaz.com/wp-content/uploads/2017/01/Indian-Emblem.jpg' alt='John' style='width:50%'>
          <h4>Name: $name </h4>
          <h4>Contact: $contact </h4>
          <h4>Lisence no: DGM20HIHG</h4>
          <h4>Valid till: $current_date </h4>
          <p><button>DGM Mizoram</button></p>
        </div>
        
        ");


        // $filePath_mining_documents = $pdf->store('images', ['disk' => 'public']);




        $content = $pdf->download('itsolutionstuff.pdf')->getOriginalContent();

        // $filePath_mining_documents = $request->file('sketch_plan')->store('images', ['disk' => 'public']);


        return Storage::put("/pdf/$contact.pdf", $content);

        // return Storage::put('/pdf/name.pdf',$content);
    }

    public function diffDate()
    {

        // return Lisence::where('expiry_date',Carbon::now()->toDateString())->get();
        $lisence = Lisence::where('expiry_date', Carbon::now()->subDays(3)->toDateString())->first();
        return $lisence->id;
        // return Carbon::now()->toDateString();
        // return Lisence::all()->pluck('expiry_date');
    }

    public function index()
    {
        // return DB::select("select * from applicants a,individual_sectors i,private_sectors pr,public_sectors pu where a.id = i.applicant_id or a.id = pr.applicant_id or a.id = pu.applicant_id");
        return Applicant::orderBy('created_at','desc')->get();
    }



    public function getSectors($sectors, $id)
    {
        if ($sectors == 'individual') {
            return IndividualSector::where('applicant_id', $id);
        }

        if ($sectors == 'private') {
            return IndividualSector::where('applicant_id', $id);
        }

        if ($sectors == 'public') {
            return PublicSector::where('applicant_id', $id);
        }

        if ($sectors == 'private') {
            return CooperativeSector::where('applicant_id', $id);
        }
    }

    public function dashboard()
    {
        $total_applicants = $this->getTotalLisences();

        $total_expired = $this->getTotalExpired();

        $total_renewed = $this->getTotalRenewed();

        $weekly_applicants = $this->getWeeklyApplicant();

        $weekly_expired = $this->getWeeklyExpired();

        $weekly_renewed = $this->getWeeklyRenewed();


        $recent = Applicant::orderBy('created_at', 'desc')->take(5)->get();

        




        // $dashboard = [  $total_applicants, $total_expired, $total_renewed, $weekly_applicants,$weekly_expired,$weekly_renewed ];

        return response()->json([
            'total_applicants' => $total_applicants,
            'total_expired' => $total_expired,
            'total_renewed' => $total_renewed,

            'weekly_applicants' => $weekly_applicants,
            'weekly_expired' => $weekly_expired,
            'weekly_renewed' => $weekly_renewed,
            'recent' => $recent,
           


        ]);
    }

    public function getWeeklyLisence()
    {
        return Lisence::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
    }

    public function getWeeklyApplicant()
    {
        // return Carbon::now()->endOfWeek()->toDateString();
        return Applicant::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        // return Carbon::now()->endOfWeek();
    }

    public function getWeeklyRenewed()
    {
        return Renewal::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
    }

    public function getWeeklyExpired()
    {
        return Lisence::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
            ->where('status', 'expired')->count();
    }

    public function getTotalApplicants()
    {
        return Applicant::count();
    }

    public function getTotalLisences()
    {
        return Lisence::count();
    }

    public function getTotalRenewed()
    {
        return Renewal::count();
    }

    public function getTotalExpired()
    {
        return Lisence::where('status', 'expired')->count();
    }

    public function generateLisence(Request $request)
    {

        // return $request->id;

        $lisence = new Lisence();

        $lisence->applicant_id = $request->id;

        $lisence->lisence_id = 'abc';

        $lisence->status = 'active';

        $lisence->expiry_date = Carbon::now()->addYear(1);

        $lisence->save();

        return Applicant::where('id', $request->id)
            ->update(['status' => 'active']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function monthly()
    {
        // return Applicant::orderBy('created_at','desc')->groupBy(DB::raw('MONTH(created_at)'));
        $users = Lisence::select('id', 'created_at')
            ->get()
            ->groupBy(function ($date) {
                //return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                return Carbon::parse($date->created_at)->format('m'); // grouping by months
            });

        return $users;
    }


    public function searcActive($any = null)
    {
        if (!$any)
            return Applicant::all();

        $applicant = Applicant::where('name', 'LIKE', '%' . $any . '%')
            ->where('status', 'active')
            ->orWhere('contact', 'LIKE', '%' . $any . '%')->get();
        return $applicant;
    }


    public function searchExpired($any)
    {
        if (!$any)
            return Applicant::all();

        $applicant = Applicant::where('name', 'LIKE', '%' . $any . '%')
            ->where('status', 'expired')
            ->orWhere('contact', 'LIKE', '%' . $any . '%')->get();
        return $applicant;
    }


    public function searchRenewed($any)
    {
        if (!$any)
            return Applicant::all();

        $applicant = Applicant::where('name', 'LIKE', '%' . $any . '%')
            ->where('lisence_renewed', 1)
            ->orWhere('contact', 'LIKE', '%' . $any . '%')->get();
        return $applicant;
    }


    public function searchApplicants($any)
    {
        if (!$any)
            return Applicant::all();

        $applicant = Applicant::where('name', 'LIKE', '%' . $any . '%')
            ->orWhere('contact', 'LIKE', '%' . $any . '%')->get();
        return $applicant;
    }

    public function generatePdf($name, $contact, $expiry, $lisence = null)
    {
        if (!$lisence)
            $lisence = "DGM" . date("Y") . Str::random(4) . rand(100, 200);

        $lisence = strtoupper($lisence);

        // $current_date = Carbon::now()->addYear(1)->toDateString();

        $pdf = PDF::loadHTML("
        <style>
        .card {
          box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
          max-width: 300px;
          margin: auto;
          text-align: center;
          font-family: arial;
          background-color: rgb(248, 248, 248);
        }
        
        .title {
          color: grey;
          font-size: 18px;
        }
        
        button {
          border: none;
          outline: 0;
          display: inline-block;
          padding: 8px;
          color: white;
          background-color: #000;
          text-align: center;
          cursor: pointer;
          width: 100%;
          font-size: 18px;
        }
        
        a {
          text-decoration: none;
          font-size: 22px;
          color: black;
        }
        
        button:hover, a:hover {
          opacity: 0.7;
        }
        </style>
        </head>
        <body>
        
        
        
        <div class='card'>
          <img src='https://www.ritiriwaz.com/wp-content/uploads/2017/01/Indian-Emblem.jpg' alt='John' style='width:50%'>
          <h4>Name: $name </h4>
          <h4>Contact: $contact </h4>
          <h4>Licence no: $lisence </h4>
          <h4>Valid till: $expiry </h4>
          <p><button>DGM Mizoram</button></p>
        </div>
        
        ");


        $content = $pdf->download('lailen.pdf')->getOriginalContent();

        // $filePath_mining_documents = $request->file('sketch_plan')->store('images', ['disk' => 'public']);

        // return Storage::put('/images/'+$request->contact+'.pdf',$content);

        $random = $contact . Str::random(5);

        Storage::put("/pdf/$random.pdf", $content);

        return $random . '.pdf';




        $lisc = "DGM" . date("Y") . Str::random(4) . rand(100, 200);


        $lisence = new Lisence();

        $lisence->applicant_id = $this->id;

        $lisence->lisence_id = $lisc;

        $lisence->status = 'active';

        $lisence->expiry_date = Carbon::now()->addYear(1);

        $lisence->file = $random . '.pdf';

        $lisence->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        //    $request->validate([
        //        'lisence_id' => 'unique:lisences,lisence_id'
        //    ]);

        $this->id = DB::table('applicants')->max('id');

        if (!$this->id)
            $this->id = 0;

        $this->id  = $this->id  +  1;

        $applicant = new Applicant();

        $applicant->id = $this->id;

        $applicant->name = $request->name;

        $applicant->village = $request->village;

        $applicant->houseno = $request->houseno;

        $applicant->contact = $request->contact;

        $applicant->sd = $request->sd;

        $applicant->telephone = $request->telephone;

        $applicant->status = 'active';

        $applicant->sector_type = $request->sector_type;

        $applicant->business_profession = $request->business_profession;

        $applicant->minerals_intended = $request->minerals_intended;

        $applicant->mining_period = $request->mining_period;

        $applicant->area_extent = $request->area_extent;

        $applicant->area_details = $request->area_details;

        $applicant->under_forest = $request->under_forest == 'false' ? 0 : 1;

        $applicant->forest_division = $request->forest_division;

        $applicant->legal_status = $request->legal_status;

        $applicant->sanctuary =  $request->sanctuary == 'false' ? 0 : 1;

        $applicant->type_extent = $request->type_extent;

        $applicant->already_hold_already = $request->already_hold_already;

        $applicant->already_applied_not_granted = $request->already_applied_not_granted;

        $applicant->applied_simultaneously = $request->applied_simultaneously;

        $applicant->captive_use = $request->captive_use;

        $applicant->if_sale = $request->if_sale;

        $applicant->lisence_renewed = 0;

        $applicant->financial_resource = $request->financial_resource;


        $filePath_mining_documents = $request->file('mining_documents')->store('images', ['disk' => 'public']);

        $filePath_sketch_plan = $request->file('sketch_plan')->store('images', ['disk' => 'public']);

        $filePath_other_particulars = $request->file('other_particulars')->store('images', ['disk' => 'public']);


        $applicant->mining_documents = $filePath_mining_documents;

        $applicant->sketch_plan = $filePath_sketch_plan;

        $applicant->other_particulars = $filePath_other_particulars;

        $applicant->district = $request->district;

        $applicant->mouza = $request->mouza;

        $applicant->city_town = $request->city_town;

        $applicant->plot_no = $request->plot_no;

        $applicant->areas = $request->areas;

        $applicant->ownership = $request->ownership;

        $applicant->save();

        // return;



        if ($request->sector_type == 'individual') {

            $indvidual = new IndividualSector();
            $indvidual->applicant_id = $this->id;
            $indvidual->individual_nationality = $request->individual_nationality;
            $indvidual->individual_residency = $request->individual_residency;
            $indvidual->individual_qualification = $request->individual_qualification;

            $indvidual->save();
        }
        if ($request->sector_type == 'private') {
            $filePath_private_regd_place = $request->file('private_certificate')->store('images', ['disk' => 'public']);
            $privateSector = new PrivateSector();
            $privateSector->applicant_id = $this->id;
            $privateSector->private_regd_place = $request->private_regd_place;
            $privateSector->private_regd_certificate = $filePath_private_regd_place;
            $privateSector->private_members_nationality = $request->private_members_nationality;
            $privateSector->save();
        }

        if ($request->sector_type == 'public') {
            $filePath_public_certificate = $request->file('public_certificate')->store('images', ['disk' => 'public']);
            $publicSector = new PublicSector();
            $publicSector->applicant_id = $this->id;
            $publicSector->public_incorporation_place = $request->public_corporation_place;
            $publicSector->public_incorporation_certificate = $filePath_public_certificate;
            $publicSector->public_directors_nationality = $request->public_nationality;
            $publicSector->save();
        }

        if ($request->sector_type == 'cooperative') {
            // return 'cooopeer';
            $filePath_cooperative_certificate = $request->file('cooperative_certificate')->store('images', ['disk' => 'public']);
            $cooperative = new CooperativeSector();
            $cooperative->applicant_id = $this->id;
            $cooperative->cooperative_nationality = $request->cooperative_nationality;
            $cooperative->cooperative_regd_place = $request->cooperative_regd_place;
            $cooperative->cooperative_certificate = $filePath_cooperative_certificate;
            $cooperative->save();
        }

        if ($request->sector_type == 'firm') {
            // return 'firm';
            $firm = new FirmAssociationSector();
            $firm->applicant_id = $this->id;
            $firm->firm_nationality = $request->firm_nationality;
            $firm->firm_members = $request->firm_members;
            $firm->save();
        }

        // return;
        //Generate pdf

        $lisence = strtoupper("DGM" . date("Y") . Str::random(4) . rand(100, 200));


        $current_date = Carbon::now()->addYear(1)->toDateString();

        $pdf = PDF::loadHTML("
        <style>
        .card {
          box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
          max-width: 300px;
          margin: auto;
          text-align: center;
          font-family: arial;
          background-color: rgb(248, 248, 248);
        }
        
        .title {
          color: grey;
          font-size: 18px;
        }
        
        button {
          border: none;
          outline: 0;
          display: inline-block;
          padding: 8px;
          color: white;
          background-color: #000;
          text-align: center;
          cursor: pointer;
          width: 100%;
          font-size: 18px;
        }
        
        a {
          text-decoration: none;
          font-size: 22px;
          color: black;
        }
        
        button:hover, a:hover {
          opacity: 0.7;
        }
        </style>
        </head>
        <body>
        
        
        
        <div class='card'>
          <img src='https://www.ritiriwaz.com/wp-content/uploads/2017/01/Indian-Emblem.jpg' alt='John' style='width:50%'>
          <h4>Name: $request->name </h4>
          <h4>Contact: $request->contact </h4>
          <h4>Licence no: $lisence </h4>
          <h4>Valid till: $current_date </h4>
          <p><button>DGM Mizoram</button></p>
        </div>
        
        ");


        // $filePath_mining_documents = $pdf->store('images', ['disk' => 'public']);




        $content = $pdf->download('lailen.pdf')->getOriginalContent();

        // $filePath_mining_documents = $request->file('sketch_plan')->store('images', ['disk' => 'public']);

        // return Storage::put('/images/'+$request->contact+'.pdf',$content);

        $random = $request->contact . Str::random(5);

        Storage::put("/pdf/$random.pdf", $content);




        $lisc = $lisence;


        $lisence = new Lisence();

        $lisence->applicant_id = $this->id;

        $lisence->lisence_id = $lisc;

        $lisence->status = 'active';

        $lisence->expiry_date = Carbon::now()->addYear(1);

        $lisence->file = $random . '.pdf';

        $lisence->save();

        // return Applicant::where('id', $request->id)
        //     ->update(['status' => 'active']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return 'hellxo';

        $applicant = Applicant::find($id);

        // return $applicant;

        if ($applicant->sector_type == 'firm') {
            // return DB::select("select * from firm_association_sectors");
            $sectors = 'firm_association_sectors';
        } else {

            $sectors  = $applicant->sector_type . '_sectors';
        }

        // return $sectors;



        $query = DB::select("select * from applicants, $sectors where applicants.id = $applicant->id and $sectors.applicant_id = $applicant->id");


        return $query;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $applicant = Applicant::find($id);

        $sectors  = $applicant->sector_type . '_sectors';


        $query = DB::select("select * from applicants, $sectors where applicants.id = $sectors.applicant_id");


        return $query;
    }

    public function storeRenewal(Request $request, $id)
    {


        $filePath_clearance_certificate = $request->file('clearance_certificate')->store('images', ['disk' => 'public']);

        $filePath_mining_plan = $request->file('mining_plan')->store('images', ['disk' => 'public']);

        $filePath_other_particulars = $request->file('other_particulars')->store('images', ['disk' => 'public']);

        $renewal = new Renewal();

        $renewal->applicant_id = $id;

        $renewal->name = $request->name;

        $renewal->sd = $request->sd;

        $renewal->name = $request->name;

        $renewal->village = $request->village;

        $renewal->houseno = $request->houseno;

        $renewal->contact = $request->contact;

        $renewal->telephone = $request->telephone;

        $renewal->clearance_certificate = $filePath_clearance_certificate;

        $renewal->sector_type = $request->sector_type;

        $renewal->business_profession = $request->business_profession;

        $renewal->desired_renewal = $request->desired_renewal;

        $renewal->previous_renewal = $request->previous_renewal;

        $renewal->renewal_period = $request->renewal_period;

        $renewal->already_hold = $request->already_hold;

        $renewal->already_applied = $request->already_applied;

        $renewal->applied_simultaneously = $request->applied_simultaneously;

        $renewal->captive_use = $request->captive_use;

        $renewal->if_sale = $request->if_sale;

        $renewal->mining_lease_granted_earlier = $request->mining_lease_granted_earlier;

        $renewal->area_applied_for = $request->area_applied_for;

        $renewal->renewal_desc = $request->renewal_desc;

        $renewal->no_objection = $request->no_objection;

        $renewal->output_details = $request->output_details;

        $renewal->mining_plan = $filePath_mining_plan;

        $renewal->other_particulars = $filePath_other_particulars;

        $renewal->save();

        $applicant = Applicant::find($id);

        $applicant->lisence_renewed = 1;

        $applicant->status = 'active';

        $applicant->save();

        $lisence = Lisence::where('applicant_id', $id)->first();

        $expiry_date = Carbon::parse($lisence->expiry_date);

        $file = $this->generatePdf($request->name, $request->contact, $expiry_date->addYear(1), $lisence->lisence_id);

        Lisence::where('applicant_id', $id)
            ->update([
                'expiry_date' => $expiry_date,
                'lisence_renewed' => $lisence->lisence_renewed + 1,
                'file' => $file
            ]);
    }

    public function getRenewedLisenceList($id)
    {
        // return 'hellorenew';

        return DB::select("select * from applicants,renewals where applicants.id = $id and applicants.lisence_renewed = 1 and renewals.applicant_id = $id order by renewals.created_at desc");
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->file('sketch_plan');

        // if ($request->file('public_incorporation_certificate')) {
        //     return 'exist';
        // } 
        // return 'not exist';



        $applicant = Applicant::find($id);

        $applicant->name = $request->name;

        $applicant->village = $request->village;

        $applicant->houseno = $request->houseno;

        $applicant->telephone = $request->telephone;

        // $applicant->status = 'pending';

        $applicant->sector_type = $request->sector_type;

        $applicant->business_profession = $request->business_profession;

        $applicant->minerals_intended = $request->minerals_intended;

        $applicant->mining_period = $request->mining_period;

        $applicant->area_extent = $request->area_extent;

        $applicant->area_details = $request->area_details;

        $applicant->under_forest = $request->under_forest == 'false' ? 0 : 1;;

        $applicant->forest_division = $request->forest_division;

        $applicant->legal_status = $request->legal_status;

        $applicant->sanctuary = $request->sanctuary == 'false' ? 0 : 1;

        $applicant->type_extent = $request->type_extent;

        $applicant->already_hold_already = $request->already_hold_already;

        $applicant->already_applied_not_granted = $request->already_applied_not_granted;

        $applicant->applied_simultaneously = $request->applied_simultaneously;

        $applicant->captive_use = $request->captive_use;

        $applicant->if_sale = $request->if_sale;

        $applicant->financial_resource = $request->financial_resource;

        $applicant->district = $request->district;

        $applicant->mouza = $request->mouza;

        $applicant->city_town = $request->city_town;

        $applicant->plot_no = $request->plot_no;

        $applicant->areas = $request->areas;

        $applicant->ownership = $request->ownership;



        if ($request->file('mining_documents')) {

            $filePath_mining_documents = $request->file('mining_documents')->store('images', ['disk' => 'public']);
            $applicant->mining_documents = $filePath_mining_documents;
        }


        if ($request->file('sketch_plan')) {

            $filePath_sketch_plan = $request->file('sketch_plan')->store('images', ['disk' => 'public']);
            $applicant->sketch_plan = $filePath_sketch_plan;
        }

        if ($request->file('other_particulars')) {

            $filePath_other_particulars = $request->file('other_particulars')->store('images', ['disk' => 'public']);
            $applicant->other_particulars = $filePath_other_particulars;
        }


        $applicant->save();


        if ($request->sector_type == 'individual') {
            // return 'individual';
            $indvidual = IndividualSector::where('applicant_id', $id)->first();
            if ($indvidual) {
                // return 'exist';
                IndividualSector::where('applicant_id', $id)
                    ->update([
                        'individual_nationality' => $request->individual_nationality,
                        'individual_residency' => $request->individual_residency,
                        'individual_qualification' => $request->individual_qualification
                    ]);
            } else {
                // return 'not exist';
                $newIndividual = new IndividualSector();
                $newIndividual->applicant_id = $id;
                $newIndividual->individual_nationality = $request->individual_nationality;
                $newIndividual->individual_residency = $request->individual_residency;
                $newIndividual->individual_qualification = $request->individual_qualification;
                $newIndividual->save();
            }
        }
        if ($request->sector_type == 'private') {

            $privateSector = PrivateSector::where('applicant_id', $id)->first();

            if ($privateSector) {
                if ($request->file('private_regd_certificate')) {

                    $filePath_private_regd_place = $request->file('private_regd_certificate')->store('images', ['disk' => 'public']);


                    PrivateSector::where('applicant_id', $id)
                        ->update([
                            'private_regd_place' => $request->private_regd_place,
                            'private_regd_certificate' => $filePath_private_regd_place,
                            'private_members_nationality' => $request->private_members_nationality
                        ]);
                } else {

                    PrivateSector::where('applicant_id', $id)
                        ->update([
                            'private_regd_place' => $request->private_regd_place,
                            'private_members_nationality' => $request->private_members_nationality
                        ]);
                }
            } else {
                $filePath_private_regd_place = $request->file('private_regd_certificate')->store('images', ['disk' => 'public']);
                $newPrivate =  new PrivateSector();
                $newPrivate->applicant_id = $id;
                $newPrivate->private_regd_place = $request->private_regd_place;
                $newPrivate->private_regd_certificate = $filePath_private_regd_place;
                $newPrivate->private_members_nationality = $request->private_members_nationality;
                $newPrivate->save();
            }
        }

        if ($request->sector_type == 'public') {
            if ($request->file('public_incorporation_certificate'))

                $filePath_public_certificate = $request->file('public_incorporation_certificate')->store('images', ['disk' => 'public']);

            $publicSector = PublicSector::where('applicant_id', $id)->first();

            if ($publicSector) {

                if ($request->file('public_incorporation_certificate')) {

                    PublicSector::where('applicant_id', $id)
                        ->update([
                            'public_incorporation_place' => $request->public_incorporation_place,
                            'public_incorporation_certificate' => $filePath_public_certificate,
                            'public_directors_nationality' => $request->public_directors_nationality
                        ]);
                } else {
                    PublicSector::where('applicant_id', $id)
                        ->update([
                            'public_incorporation_place' => $request->public_incorporation_place,
                            'public_directors_nationality' => $request->public_directors_nationality
                        ]);
                }
            } else {
                $newPublic = new PublicSector();
                $newPublic->applicant_id = $id;
                $newPublic->public_incorporation_place = $request->public_incorporation_place;
                $newPublic->public_incorporation_certificate = $filePath_public_certificate;
                $newPublic->public_directors_nationality = $request->public_directors_nationality;
                $newPublic->save();
            }
        }

        if ($request->sector_type == 'cooperative') {
            // return 'cooperative';


            if ($request->file('cooperative_certificate'))
                $filePath_cooperative_certificate = $request->file('cooperative_certificate')->store('images', ['disk' => 'public']);

            $coopSector = CooperativeSector::where('applicant_id', $id)->first();

            if ($coopSector) {
                if ($request->file('cooperative_certificate')) {

                    CooperativeSector::where('applicant_id', $id)
                        ->update([
                            'cooperative_nationality' => $request->cooperative_nationality,
                            'cooperative_regd_place' => $request->cooperative_regd_place,
                            'cooperative_certificate' => $filePath_cooperative_certificate
                        ]);
                } else {
                    CooperativeSector::where('applicant_id', $id)
                        ->update([
                            'cooperative_nationality' => $request->cooperative_nationality,
                            'cooperative_regd_place' => $request->cooperative_regd_place
                        ]);
                }
            } else {
                $newCoop =  new CooperativeSector();
                $newCoop->applicant_id = $id;
                $newCoop->cooperative_nationality = $request->cooperative_nationality;
                $newCoop->cooperative_regd_place = $request->cooperative_regd_place;
                $newCoop->cooperative_certificate = $filePath_cooperative_certificate;
                $newCoop->save();
            }
        }

        if ($request->sector_type == 'firm') {
            // return FirmAssociationSector::all();
            $firmSector = FirmAssociationSector::where('applicant_id', $id)->first();

            if ($firmSector) {

                FirmAssociationSector::where('applicant_id', $id)
                    ->update([
                        'firm_nationality' => $request->firm_nationality,
                        'firm_members' => $request->firm_members
                    ]);
            } else {
                $newFirm =  new FirmAssociationSector();
                $newFirm->applicant_id = $id;
                $newFirm->firm_nationality = $request->firm_nationality;
                $newFirm->firm_members = $request->firm_members;
                $newFirm->save();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $applicant = Applicant::find($id);
        $applicant->delete();
    }

    public function getActiveLisence()
    {
        // return 'renewsss';
        return DB::select("select * from applicants,lisences where applicants.id = lisences.applicant_id order by lisences.created_at desc");

        // return DB::select("select * from applicants,lisences where applicants.id = lisences.applicant_id ");
        // return DB::table('Applicants as app')
        //     ->join('lisence as lsc', 'app.id', '=','lsc.applicant_id')
        //     ->get();
    }

    public function showLisence($id)
    {
        // return "hello";
        $lisence = Lisence::where('lisence_id', $id)->first();

        return DB::select("select * from applicants,lisences where applicants.id = $lisence->applicant_id and lisences.applicant_id = $lisence->applicant_id");
    }

    public function getRenewedLisence()
    {
        return DB::select("select * from applicants,lisences where lisence_renewed = 1 ");
        // return DB::table('Applicants as app')
        //     ->join('lisence as lsc', 'app.id', '=','lsc.applicant_id')
        //     ->get();
    }

    public function getCount()
    {

        // return Renewal::select("select created_at, count(created_at) as Orders
        // from renewals td
        //   left join Orders o
        //     on date(o.OrderDate) = db_date
        // where td.db_date >= date(adddate(curdate(), interval -30 day))
        //   and td.db_date <= date(curdate())
        // group by td.db_date");



        return Renewal::select('id', 'created_at')->get()
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('d');
            });
    }



    public function getExpiredLisence()
    {
        return DB::select("select * from applicants where status = 'expired'");
    }

    public function getPendingLisence()
    {
        return Applicant::where('status', 'pending');
    }


    public function updateRenewal($id)
    {

        return DB::select("select * from renewals where id = $id");
    }

    public function renewLisence($id)
    {
        // return "jjj";
        return DB::select("select * from renewals where applicant_id = $id");
    }

    public function updatedRenewal(Request $request, $id)
    {

        // return  $request->file('clearance_certificate');
        // return 'hello';


        if ($request->file('clearance_certificate')) {

            $filePath_clearance_certificate = $request->file('clearance_certificate')->store('images', ['disk' => 'public']);
        }


        if ($request->file('mining_plan')) {

            $filePath_mining_plan = $request->file('mining_plan')->store('images', ['disk' => 'public']);
        }

        if ($request->file('other_particulars')) {

            $filePath_other_particulars = $request->file('other_particulars')->store('images', ['disk' => 'public']);
        }

        // $filePath_clearance_certificate = $request->file('clearance_certificate')->store('images', ['disk' => 'public']);

        // $filePath_mining_plan = $request->file('mining_plan')->store('images', ['disk' => 'public']);

        // $filePath_other_particulars = $request->file('other_particulars')->store('images', ['disk' => 'public']);

        $renewal = Renewal::find($id);


        Renewal::where('id', $id)
            ->update([
                'name' => $request->name,
                'sd' => $request->sd,
                'name' => $request->name,
                'village' => $request->village,
                'houseno' => $request->houseno,
                'contact' => $request->contact,
                'telephone' => $request->telephone,
                'sector_type' => $request->sector_type,
                'business_profession' => $request->business_profession,
                'desired_renewal' => $request->desired_renewal,
                'previous_renewal' => $request->previous_renewal,
                'renewal_period' => $request->renewal_period,
                'already_hold' => $request->already_hold,
                'already_applied' => $request->already_applied,
                'applied_simultaneously' => $request->applied_simultaneously,
                'captive_use' => $request->captive_use,
                'desired_renewal' => $request->desired_renewal,


                'clearance_certificate' => $request->file('clearance_certificate') ? $filePath_clearance_certificate : $renewal->clearance_certificate,
                'mining_plan' => $request->file('mining_plan') ? $filePath_mining_plan : $renewal->mining_plan,
                'other_particulars' => $request->file('other_particulars') ? $filePath_other_particulars : $renewal->other_particulars,



                'if_sale' => $request->if_sale,
                'mining_lease_granted_earlier' => $request->mining_lease_granted_earlier,
                'area_applied_for' => $request->area_applied_for,
                'renewal_desc' => $request->renewal_desc,
                'no_objection' => $request->no_objection,
                'output_details' => $request->output_details
            ]);
    }
}
