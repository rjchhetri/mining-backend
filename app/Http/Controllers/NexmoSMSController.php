<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NexmoSMSController extends Controller
{
    public function index() {
        $basic  = new \Vonage\Client\Credentials\Basic("6a081506", "Zt5XCbSiCccFiwsI");
        $client = new \Vonage\Client($basic);

        $response = $client->sms()->send(
            new \Vonage\SMS\Message\SMS("916009432646", 'BRAND_NAME', 'A text message sent using the Nexmo SMS API')
        );
        
        $message = $response->current();
        
        if ($message->getStatus() == 0) {
            echo "The message was sent successfully\n";
        } else {
            echo "The message failed with status: " . $message->getStatus() . "\n";
        }
    }
}
